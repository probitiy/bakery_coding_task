# Coding Challenge Solution
# Requirements
- Ruby = 2.4.3

# Setup
- Goto project's folder in your terminal
- Install proper ruby version (".ruby-version" file is for rbenv and rvm users)
- run `bundle install`
- now run `ruby bin/bakery_runner.rb`
- enter item count and item code separated by space symbol

##Run with example file to quickly check algorithm work
- run `ruby bin/bakery_runner.rb < examples/example_input.txt` for successful output example
### Expected result:
> Please enter Order Request in format: "<Quantity> <Item Code>"

> 10 VS5 $17.98

> 2 X 5 8.99

> 14 MB11 $54.75

> 3 X 2 9.95

> 1 X 8 24.9

> 13 CF $25.85

> 1 X 3 5.95

> 2 X 5 9.95

# Specs
if you want to run the specs please run `rspec .` in project's home directory.

# Code styling (Rubocop)
- run `ruby bin/bakery_runner.rb < examples/example_input.txt` for successful output example
### Expected result:
> 21 files inspected, no offenses detected

