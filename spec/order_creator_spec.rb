# frozen_string_literal: true

require 'spec_helper'

describe OrderCreator do
  let(:vs_package_3) { Package.new(3, 6.99) }
  let(:vs_package_5) { Package.new(5, 8.99) }
  let(:vs_package_9) { Package.new(9, 9.95) }
  let(:vs_item) do
    item = Item.new('Vegermite Scroll', 'VS5')
    item.add_package(vs_package_3)
    item.add_package(vs_package_5)
    item.add_package(vs_package_9)
    item
  end
  let(:order_request) { OrderRequest.new(10, vs_item) }

  describe '.constructor' do
    it 'does not raise error on initialization' do
      expect { described_class.new(order_request) }
    end

    context '#order_data format' do
      it 'raises an error if order_request is not an OrderRequest' do
        order_request = 'str'
        expect do
          described_class.new(order_request)
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end

      it 'raises an error if order_request is nil' do
        order_request = nil
        expect do
          described_class.new(order_request)
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end
    end
  end

  describe '.create_order' do
    it 'successfully creates order' do
      order = described_class.new(order_request).create_order
      expect(order.is_a?(Order)).to be_truthy
    end
  end

  context '#private methods' do
    describe '.find_packages_minimum' do
      let(:ni_package_6) { Package.new(1, 6.99) }
      let(:ni_package_4) { Package.new(4, 8.99) }
      let(:ni_package_1) { Package.new(6, 9.95) }
      let(:new_item) do
        item = Item.new('New Item', 'NI1')
        item.add_package(ni_package_1)
        item.add_package(ni_package_4)
        item.add_package(ni_package_6)
        item
      end

      it 'successfully returns result' do
        order_creator = described_class.new(order_request)
        minimal_packages_result = order_creator.send :find_packages_minimum,
                                                     vs_item.packages,
                                                     10

        expect(minimal_packages_result).to eq([SearchResultItem.new(0, vs_package_3),
                                               SearchResultItem.new(2, vs_package_5)])
      end

      it 'returns nil if not possible to compose' do
        order_request = OrderRequest.new(1, vs_item)
        order_creator = described_class.new(order_request)
        minimal_packages_result = order_creator.send :find_packages_minimum,
                                                     vs_item.packages,
                                                     1
        expect(minimal_packages_result).to be_nil
      end

      it 'returns result with minimal packages' do
        order_creator = described_class.new(order_request)
        minimal_packages_result = order_creator.send :find_packages_minimum,
                                                     new_item.packages,
                                                     8
        expect(minimal_packages_result).to eq([SearchResultItem.new(0, ni_package_1),
                                               SearchResultItem.new(2, ni_package_4)])
      end
    end

    describe '.create_order' do
      it 'successfully returns result' do
        order_creator = described_class.new(order_request)
        results = order_creator.create_order

        expect(results.is_a?(Order)).to be_truthy
      end

      it 'does not raise error in case of unprocessible entity' do
        order_request = OrderRequest.new(1, vs_item)
        order_creator = described_class.new(order_request)
        expect { order_creator.create_order }.to_not raise_error
      end

      it 'returns Order with nil packages value in case of unprocessible entity' do
        order_request = OrderRequest.new(1, vs_item)
        order_creator = described_class.new(order_request)
        result = order_creator.create_order

        expect(result.result_items).to eq(nil)
      end
    end
  end
end
