# frozen_string_literal: true

require 'spec_helper'

describe Item do
  let(:package_3) { Package.new(3, 6.99) }
  let(:package_5) { Package.new(5, 8.99) }
  let(:package_9) { Package.new(9, 9.95) }
  let!(:vs_item) do
    item = Item.new('Vegermite Scroll', 'VS5')
    item.add_package(package_3)
    item.add_package(package_5)
    item.add_package(package_9)
    item
  end
  before do
    include BakeryValidations
  end

  describe '.constructor' do
    it 'does not raise error on initialization' do
      expect { described_class.new('New Item', 'NI1') }.to_not raise_error
    end

    context '#name_format' do
      it 'raises an error if name is not string' do
        expect { described_class.new(22, 'NI1') }.to raise_error(BakeryErrors::ForbiddenInputValue)
      end

      it 'raises an error if name is nil' do
        expect { described_class.new(nil, 'NI1') }.to raise_error(BakeryErrors::ForbiddenInputValue)
      end
    end

    context '#code_format' do
      it 'raises an error if code is not string' do
        expect do
          described_class.new('New Item', 22)
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end

      it 'raises an error if code is nil' do
        expect do
          described_class.new('New Item', nil)
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end
    end
  end

  describe '.add_package' do
    it 'returns an empty Array if no packages were added' do
      expect(described_class.new('New Item', 'NI1').packages).to eq([])
    end

    it 'successfully adds packages' do
      item = described_class.new('New Item', 'NI1')
      item.add_package(package_9)
      expect(item.packages).to eq([package_9])
    end

    it 'raises an error if the same package added' do
      item = described_class.new('New Item', 'NI1')
      item.add_package(package_9)

      expect { item.add_package(package_9) }.to raise_error(BakeryErrors::UniquenessViolation)
    end

    it 'raises an error if argument is not Package' do
      item = described_class.new('New Item', 'NI1')

      expect { item.add_package('package_9') }.to raise_error(BakeryErrors::ForbiddenInputValue)
    end
  end
end
