# frozen_string_literal: true

require 'spec_helper'

describe Package do
  let(:package) { described_class.new(4, 13.22) }

  describe '#constructor' do
    it 'raise no error on initialization' do
      expect { described_class.new(4, 13.22) }.to_not raise_error
    end

    it 'raise error if quantity is not positive' do
      expect do
        described_class.new(-4, 13.22)
      end.to raise_error(BakeryErrors::ForbiddenInputValue)
    end

    it 'raise error if quantity is not integer' do
      expect do
        described_class.new(4.11, 13.22)
      end.to raise_error(BakeryErrors::ForbiddenInputValue)
    end

    it 'raise error if quantity is string' do
      expect do
        described_class.new('str', 13.22)
      end.to raise_error(BakeryErrors::ForbiddenInputValue)
    end

    it 'raise error if quantity is Object' do
      expect do
        described_class.new(Object.new, 13.22)
      end.to raise_error(BakeryErrors::ForbiddenInputValue)
    end

    it 'raise error if price is not positive' do
      expect { described_class.new(4, -3) }.to raise_error(BakeryErrors::ForbiddenInputValue)
    end

    it 'raise error if price is zero' do
      expect { described_class.new(4, 0) }.to raise_error(BakeryErrors::ForbiddenInputValue)
    end

    it 'raise error if price is zero' do
      expect { described_class.new(4, 0) }.to raise_error(BakeryErrors::ForbiddenInputValue)
    end

    it 'raise error if price is a string' do
      expect do
        described_class.new(4, 'str')
      end.to raise_error(BakeryErrors::ForbiddenInputValue)
    end

    it 'raise error if price is an Object' do
      expect do
        described_class.new(4, Object.new)
      end.to raise_error(BakeryErrors::ForbiddenInputValue)
    end
  end
end
