# frozen_string_literal: true

require 'spec_helper'

describe Order do
  let(:package_3) { Package.new(3, 6.99) }
  let(:package_5) { Package.new(5, 8.99) }
  let(:package_9) { Package.new(9, 9.95) }
  let!(:vs_item) do
    item = Item.new('Vegermite Scroll', 'VS5')
    item.add_package(package_3)
    item.add_package(package_5)
    item.add_package(package_9)
    item
  end
  let(:package_result) { [SearchResultItem.new(2, package_5)] }
  let(:order) { described_class.new(vs_item, 10, package_result) }

  describe '.constructor' do
    it 'does not raise error on initialization with proper params' do
      expect do
        described_class.new(vs_item, 10, package_result)
      end.to_not raise_error
    end

    context '#quantity format' do
      it 'raises error if quantity is not positive' do
        expect do
          described_class.new(vs_item, -2, package_result)
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end

      it 'raises error if quantity is not integer' do
        expect do
          described_class.new(vs_item, 2.22, package_result)
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end

      it 'raises error if quantity is string' do
        expect do
          described_class.new(vs_item, 'str', package_result)
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end

      it 'raises error if quantity is Object' do
        expect do
          described_class.new(vs_item, Object.new, package_result)
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end

      it 'raises error if quantity is nil' do
        expect do
          described_class.new(vs_item, nil, package_result)
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end
    end

    context '#item format' do
      it 'raises error if item is not entity of Item' do
        expect do
          described_class.new(Object.new, 2, package_result)
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end

      it 'raises error if quantity is string' do
        expect do
          described_class.new('vs_item', 2, package_result)
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end

      it 'raises error if quantity is nil' do
        expect do
          described_class.new(nil, 2, package_result)
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end
    end

    context '#package result format' do
      it 'raises error if package result is string' do
        package_result = 'str'
        expect do
          described_class.new(vs_item, 10, package_result)
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end

      it 'raises error if package result is Object' do
        package_result = Object.new
        expect do
          described_class.new(vs_item, 10, package_result)
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end

      it 'raises error if package result is array of stings' do
        package_result = %w[aaa bbb ccc]
        expect do
          described_class.new(vs_item, 10, package_result)
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end

      it 'raises error if package result is array of blank arrays' do
        package_result = [[], [], []]
        expect do
          described_class.new(vs_item, 10, package_result)
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end

      it 'does not raise error if package result is correct' do
        package_result = [SearchResultItem.new(9, package_9),
                          SearchResultItem.new(2, package_5),
                          SearchResultItem.new(9, package_3)]
        expect { described_class.new(vs_item, 10, package_result) }.to_not raise_error
      end

      it 'raises error if package result has the same packages' do
        package_result = [[package_9, 9], [package_5, 2], [package_5, 9]]
        expect do
          described_class.new(vs_item, 10, package_result)
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end
    end
  end

  describe '.public_methods' do
    context '#to_s' do
      it 'successfully shows formatted result' do
        expect(order.to_s).to eq("10 VS5 $17.98\n2 X 5 8.99")
      end
    end
  end

  describe '.private_methods' do
    context '#total_price' do
      it 'successfully calculates total price' do
        expect(order.send(:total_price)).to eq(17.98)
      end

      it 'returns nil if unprocessible entity' do
        unorocessible_result_item = described_class.new(vs_item, 1, nil)
        expect(unorocessible_result_item.send(:total_price)).to be_nil
      end
    end

    context '#formatted_result_items' do
      it 'successfully format package results' do
        expect(order.send(:formatted_search_result_items)).to eq('2 X 5 8.99')
      end

      it 'shows no option if unprocessible entity' do
        unorocessible_result_item = described_class.new(vs_item, 1, nil)
        expect(unorocessible_result_item.send(:formatted_search_result_items)).to eq('No option')
      end
    end

    context '#total' do
      it 'successfully generates total string' do
        expect(order.send(:total)).to eq('10 VS5 $17.98')
      end

      it 'does not show price if unprocessible entity' do
        unorocessible_result_item = described_class.new(vs_item, 1, nil)
        expect(unorocessible_result_item.send(:total)).to eq('1 VS5')
      end
    end
  end
end
