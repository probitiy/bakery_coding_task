# frozen_string_literal: true

require 'spec_helper'

describe SearchResultItem do
  let(:package) { Package.new(2, 13.22) }

  describe '.constructor' do
    it 'raise no error on initialization' do
      expect { described_class.new(4, package) }.to_not raise_error
    end

    context '#quantity_format' do
      it 'raises error if quantity is not integer' do
        expect do
          described_class.new(4.22, package)
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end

      it 'raises error if quantity is string' do
        expect do
          described_class.new('4', package)
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end

      it 'raises error if quantity is nil' do
        expect do
          described_class.new(nil, package)
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end
    end

    context '#package_format' do
      it 'raises error if package has different class' do
        expect do
          described_class.new(4, Object.new)
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end

      it 'raises error if package is string' do
        expect do
          described_class.new(4, 'package')
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end

      it 'raises error if package is nil' do
        expect do
          described_class.new(4, nil)
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end
    end
  end

  describe '.to_s' do
    it 'successfuly returns proper string' do
      expect(described_class.new(4, package).to_s).to eq('4 X 2 13.22')
    end
  end

  describe '.==' do
    it 'returns true if results are equal' do
      other_result = described_class.new(4, package)
      expect(described_class.new(4, package) == other_result).to be_truthy
    end

    it 'returns false if other result has different type' do
      other_result = Object.new
      expect(described_class.new(4, package) == other_result).to be_falsey
    end

    it 'returns false if other result has different quantity' do
      other_result = described_class.new(3, package)
      expect(described_class.new(4, package) == other_result).to be_falsey
    end

    it 'returns false if other result is nil' do
      other_result = nil
      expect(described_class.new(4, package) == other_result).to be_falsey
    end

    it 'returns false if other result has different package' do
      other_package = Package.new(2, 15)
      other_result = described_class.new(4, other_package)
      expect(described_class.new(4, package) == other_result).to be_falsey
    end
  end
end
