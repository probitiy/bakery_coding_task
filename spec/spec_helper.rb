# frozen_string_literal: true

require 'bigdecimal'
require 'utils/bakery_errors'
require 'utils/bakery_validations'
require 'utils/bakery_seed'
require 'bakery'
require 'item'
require 'order'
require 'order_creator'
require 'order_request'
require 'package'
require 'search_result_item'

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.include BakeryErrors

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups
  config.order = :random
end
