# frozen_string_literal: true

require 'spec_helper'

describe BakerySeed do
  describe '#sample data' do
    it 'does not raise an error on data seeding' do
      expect { BakerySeed.sample_data }.to_not raise_error
    end
  end
end
