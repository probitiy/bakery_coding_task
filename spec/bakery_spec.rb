# frozen_string_literal: true

require 'spec_helper'

describe Bakery do
  let(:bakery) { described_class.new }
  let(:package_3) { Package.new(3, 6.99) }
  let(:package_5) { Package.new(5, 8.99) }
  let(:package_9) { Package.new(9, 9.95) }
  let(:item) do
    item = Item.new('Example item', 'VS5')
    item.add_package(package_3)
    item.add_package(package_5)
    item.add_package(package_9)
    item
  end
  let(:user_request) { '10 VS5' }

  describe '#.onstructor' do
    it 'does not raise error on initialize' do
      described_class.new
    end
  end

  describe '.add_item' do
    it 'successfully adds an item' do
      expect { bakery.add_item(item) }.to change { bakery.items }.from([]).to([item])
    end

    it 'raises an error if the same item added' do
      bakery.add_item(item)

      expect { bakery.add_item(item) }.to raise_error(BakeryErrors::UniquenessViolation)
    end

    it 'raises an error if not item entity added' do
      expect { bakery.add_item('item') }.to raise_error(BakeryErrors::ForbiddenInputValue)
    end
  end

  describe '.make_order' do
    it 'successfully shows formatted order' do
      bakery.add_item(item)
      expect(bakery.make_order(user_request)).to eq("10 VS5 $17.98\n2 X 5 8.99")
    end

    it 'successfully minimal result' do
      bakery.add_item(item)
      expect(bakery.make_order(user_request)).to eq("10 VS5 $17.98\n2 X 5 8.99")
    end

    it 'successfully result in case of unprocessible request' do
      user_request = '1 VS5'
      bakery.add_item(item)
      expect(bakery.make_order(user_request)).to eq("1 VS5\nNo option")
    end

    it 'raises error in case of incorrect input' do
      user_request = 'aaa'
      bakery.add_item(item)
      expect { bakery.make_order(user_request) }.to raise_error(BakeryErrors::ForbiddenInputValue)
    end

    it 'raises error in case of incorrect quantity input' do
      user_request = 'aaa VS5'
      bakery.add_item(item)
      expect { bakery.make_order(user_request) }.to raise_error(BakeryErrors::ForbiddenInputValue)
    end

    it 'raises error in case of incorrect code input' do
      user_request = '1 aaa'
      bakery.add_item(item)
      expect { bakery.make_order(user_request) }.to raise_error(BakeryErrors::RecordNotFound)
    end

    it 'raises error in case of empty code input' do
      user_request = ''
      bakery.add_item(item)
      expect { bakery.make_order(user_request) }.to raise_error(BakeryErrors::ForbiddenInputValue)
    end

    it 'raises error in case of nil code input' do
      user_request = nil
      bakery.add_item(item)
      expect { bakery.make_order(user_request) }.to raise_error(BakeryErrors::ForbiddenInputValue)
    end

    it 'raises error in case of different format input' do
      user_request = [10, 'VS5']
      bakery.add_item(item)
      expect { bakery.make_order(user_request) }.to raise_error(BakeryErrors::ForbiddenInputValue)
    end
  end
end
