# frozen_string_literal: true

require 'spec_helper'

describe OrderRequest do
  let(:package_3) { Package.new(3, 6.99) }
  let(:package_5) { Package.new(5, 8.99) }
  let(:package_9) { Package.new(9, 9.95) }
  let!(:vs_item) do
    item = Item.new('Vegermite Scroll', 'VS5')
    item.add_package(package_3)
    item.add_package(package_5)
    item.add_package(package_9)
    item
  end
  let(:package_result) { [SearchResultItem.new(2, package_5)] }
  let(:order) { described_class.new(vs_item, 10, package_result) }
  let(:user_input) { '10 VS5' }

  describe '.constructor' do
    it 'does not raise error on initialization with proper params' do
      expect { described_class.new(10, vs_item) }.to_not raise_error
    end

    context '#quantity format' do
      it 'raises error if quantity is not positive' do
        expect do
          described_class.new(-2, vs_item)
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end

      it 'raises error if quantity is not integer' do
        expect do
          described_class.new(2.22, vs_item)
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end

      it 'raises error if quantity is string' do
        expect do
          described_class.new('2.22', vs_item)
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end

      it 'raises error if quantity is nil' do
        expect do
          described_class.new(nil, vs_item)
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end
    end

    context '#item format' do
      it 'raises error if item is not entity of Item' do
        expect do
          described_class.new(2, Object.new)
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end

      it 'raises error if quantity is string' do
        expect do
          described_class.new(2, 'vs_item')
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end

      it 'raises error if quantity is nil' do
        expect do
          described_class.new(2, nil)
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end
    end
  end

  describe '.class_methods' do
    context '#parse' do
      it 'does not raise error on correct parameters' do
        expect { described_class.parse(user_input, [vs_item]) }.to_not raise_error
      end

      it 'returns OrderRequest entity' do
        expect(described_class.parse(user_input, [vs_item]).is_a?(OrderRequest)).to be_truthy
      end

      it 'raises error if user data has wrong format' do
        expect do
          described_class.parse(33, [vs_item])
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end

      it 'raises error if user data is nil' do
        expect do
          described_class.parse(nil, [vs_item])
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end

      it 'raises error if items array blank' do
        expect do
          described_class.parse(user_input, [])
        end.to raise_error(BakeryErrors::RecordNotFound)
      end

      it 'raises error if item code not in the list' do
        user_input = '10 aa'
        expect do
          described_class.parse(user_input, [vs_item])
        end.to raise_error(BakeryErrors::RecordNotFound)
      end

      it 'raises error if items array is nil' do
        expect do
          described_class.parse(user_input, nil)
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end

      it 'raises error if items are not array' do
        expect do
          described_class.parse(user_input, 'nil')
        end.to raise_error(BakeryErrors::ForbiddenInputValue)
      end
    end
  end
end
