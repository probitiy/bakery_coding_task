# frozen_string_literal: true

# Top level class which nests all information and allows to make order
class Bakery
  include BakeryValidations
  attr_reader :items

  def initialize
    @items = []
  end

  def add_item(item)
    item_validation(item)

    @items.push(item)
  end

  def make_order(user_request)
    order_request = OrderRequest.parse(user_request, items)

    OrderCreator.new(order_request).create_order.to_s
  end

  private

  def item_validation(item)
    validate_entity_class(item, Item, 'Items should be have Item class')
    validate_element_uniqueness(item, 'code', @items, 'Items should be unique')
  end
end
