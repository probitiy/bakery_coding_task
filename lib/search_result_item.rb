# frozen_string_literal: true

# Strucutre of main search algorythm result
class SearchResultItem
  include BakeryValidations
  attr_reader :quantity, :package

  def initialize(quantity, package)
    constructor_params_validation(quantity, package)

    @quantity = quantity
    @package = package
  end

  def to_s
    "#{quantity} X #{package.quantity} #{package.price.to_f}"
  end

  def ==(other)
    self.class == other.class && quantity == other.quantity && package == other.package
  end

  private

  def constructor_params_validation(quantity, package)
    validate_entity_class(quantity, Integer, 'Quantity should be integer')
    validate_entity_class(package, Package, 'Package should have proper type')
  end
end
