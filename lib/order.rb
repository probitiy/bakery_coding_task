# frozen_string_literal: true

# Describes Order class which allows to do all formatting for the order
class Order
  include BakeryValidations
  attr_reader :item, :quantity, :result_items

  def initialize(item, quantity, result_items)
    constructor_params_validation(item, quantity, result_items)

    @item = item
    @quantity = quantity
    @result_items = result_items
  end

  def to_s
    [total, formatted_search_result_items].join("\n")
  end

  private

  def unprocessible_output
    'No option'
  end

  def total_price
    return unless result_items

    result_items.sum { |item| BigDecimal(item.package.price) * item.quantity }
  end

  def formatted_search_result_items
    return unprocessible_output unless result_items

    result_items.map(&:to_s).join("\n")
  end

  def total
    total = "#{quantity} #{item.code}"
    total += " $#{total_price.to_f}" if total_price
    total
  end

  def constructor_params_validation(item, quantity, result_items)
    validate_entity_class(item, Item, 'Item should be present and be Item class entity')
    validate_positive(quantity, Integer, 'Quantity should be positive integer')

    validate_result_items(result_items) if result_items
  end

  def validate_result_items(result_items)
    validate_entity_class(result_items, Array, 'Result items should be an array')

    return if result_items.all? { |el| el.is_a?(SearchResultItem) }

    raise(ForbiddenInputValue, 'Packages Result should have correct format')
  end
end
