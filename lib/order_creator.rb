# frozen_string_literal: true

# Creates an order and contains main algorythm for the test task (minimum_packages_algorithm)
class OrderCreator
  include BakeryValidations

  def initialize(order_request)
    constructor_params_validation(order_request)

    @order_request = order_request
  end

  def create_order
    result_items = find_packages_minimum @order_request.item.packages,
                                         @order_request.quantity

    compact_result_items = result_items&.select { |item| item.quantity.positive? }

    Order.new @order_request.item,
              @order_request.quantity,
              compact_result_items
  end

  private

  def find_packages_minimum(packages, quantity, result = [], min = nil)
    max_package_quantity = quantity / packages.first.quantity

    (0..max_package_quantity).to_a.reverse.each do |package_quantity|
      updated_quantity = quantity - package_quantity * packages.first.quantity
      result_item = SearchResultItem.new(package_quantity, packages.first)
      updated_result = result + [result_item]

      if packages.size > 1 && updated_quantity.positive?
        returned_result = find_packages_minimum packages[1..-1],
                                                updated_quantity,
                                                updated_result,
                                                min
        min = returned_result if returned_result
      end

      min = updated_result if updated_quantity.zero? && minimum_result?(updated_result, min)
    end

    min
  end

  def minimum_result?(result, min)
    return false unless result
    return true unless min

    min.sum(&:quantity) > result.sum(&:quantity)
  end

  def constructor_params_validation(order_request)
    validate_entity_class(order_request, OrderRequest, 'Attribute should be OrderRequest entity')
  end
end
