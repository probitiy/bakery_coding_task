# frozen_string_literal: true

# Extracted validations
module BakeryValidations
  include BakeryErrors

  private

  def validate_positive(entity, klass, message)
    validate_entity_class(entity, klass, message)

    raise(ForbiddenInputValue, message) unless entity.positive?
  end

  def validate_entity_class(entity, klass, message)
    raise(ForbiddenInputValue, message) unless entity.is_a?(klass)
  end

  def validate_element_uniqueness(record, field, collection, message)
    return unless collection.detect { |el| el.send(field) == record.send(field) }

    raise(UniquenessViolation, message)
  end
end
