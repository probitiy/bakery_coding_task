# frozen_string_literal: true

module BakeryErrors
  ForbiddenInputValue = Class.new(StandardError)
  UniquenessViolation = Class.new(StandardError)
  RecordNotFound = Class.new(StandardError)
end
