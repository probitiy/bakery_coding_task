# frozen_string_literal: true

# Default values for seeding the project
class BakerySeed
  def self.sample_data
    vs_item = Item.new('Vegermite Scroll', 'VS5')
    vs_item.add_package(Package.new(3, 6.99))
    vs_item.add_package(Package.new(5, 8.99))
    mb_item = Item.new('Blueberry Muffin', 'MB11')
    mb_item.add_package(Package.new(2, 9.95))
    mb_item.add_package(Package.new(5, 16.95))
    mb_item.add_package(Package.new(8, 24.95))
    cf_item = Item.new('Croissant', 'CF')
    cf_item.add_package(Package.new(3, 5.95))
    cf_item.add_package(Package.new(5, 9.95))
    cf_item.add_package(Package.new(9, 16.99))
    bakery = Bakery.new
    bakery.add_item(vs_item)
    bakery.add_item(mb_item)
    bakery.add_item(cf_item)

    bakery
  end
end
