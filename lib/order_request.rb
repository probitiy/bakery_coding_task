# frozen_string_literal: true

# Parses user input to proper data
class OrderRequest
  include BakeryValidations
  attr_reader :quantity, :code, :item

  class << self
    def parse(user_data, items)
      unless user_data.is_a?(String)
        raise(BakeryErrors::ForbiddenInputValue, 'User data should be String')
      end
      raise(BakeryErrors::ForbiddenInputValue, 'Items should be an array') unless items.is_a?(Array)

      quantity, code = parse_params(user_data)

      item = find_item(code, items)

      new(quantity, item)
    end

    private

    def parse_params(user_data)
      quantity, code = user_data.split(/\s/)

      raise(BakeryErrors::ForbiddenInputValue, 'Code should be String') unless code.is_a?(String)
      if quantity.to_i <= 0
        raise(BakeryErrors::ForbiddenInputValue, 'Quantity should be greater than 0')
      end

      [quantity.to_i, code]
    end

    def find_item(code, items)
      item = items.detect { |itm| itm.code == code }

      raise(BakeryErrors::RecordNotFound, 'Item with such code is not present') unless item

      item
    end
  end

  def initialize(quantity, item)
    constructor_params_validation(quantity, item)

    @quantity = quantity
    @item = item
  end

  private

  def constructor_params_validation(quantity, item)
    validate_positive(quantity, Integer, 'Quantity should be integer')
    validate_entity_class(item, Item, 'Item should have proper class')
  end
end
