# frozen_string_literal: true

# Describes bakery packages
class Package
  include BakeryValidations
  attr_reader :quantity, :price

  def initialize(quantity, price)
    constructor_params_validation(quantity, price)

    @quantity = quantity
    @price = BigDecimal(price, 10)
  end

  def constructor_params_validation(quantity, price)
    validate_positive(quantity, Integer, 'Package Quantity should be positive integer')
    validate_positive(price, Numeric, 'Package Price should be positive number')
  end
end
