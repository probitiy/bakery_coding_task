# frozen_string_literal: true

# Describes bakery items
class Item
  include BakeryValidations
  attr_reader :name, :code, :packages
  def initialize(name, code)
    constructor_params_validation(name, code)

    @name = name
    @code = code
    @packages = []
  end

  def add_package(package)
    package_uniqueness_validation(package)

    @packages.push(package)
  end

  private

  def package_uniqueness_validation(package)
    validate_entity_class(package, Package, 'Packages list should include only packages')
    validate_element_uniqueness(package, 'quantity', @packages, 'Packages should be unique')
  end

  def constructor_params_validation(name, code)
    validate_entity_class(name, String, 'Name should be string')
    validate_entity_class(code, String, 'Code should be string')
  end
end
