# frozen_string_literal: true

require 'optparse'
require 'bigdecimal'
require_relative '../lib/utils/bakery_errors'
require_relative '../lib/utils/bakery_validations'
require_relative '../lib/utils/bakery_seed'
require_relative '../lib/bakery'
require_relative '../lib/item'
require_relative '../lib/order'
require_relative '../lib/order_request'
require_relative '../lib/order_creator'
require_relative '../lib/package'
require_relative '../lib/search_result_item'

# Main module to run the programm
module BakeryRunner
  include BakeryErrors

  def self.run
    bakery = BakerySeed.sample_data
    print 'Please enter Order Request in format: "<Quantity> <Item Code>"'
    puts
    ARGF.each do |line|
      begin
        print bakery.make_order(line)
      rescue StandardError => e
        print 'Error: ' + e.message
      end
      puts
    end
  end
end

BakeryRunner.run
